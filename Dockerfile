FROM ruby:3.2.2

WORKDIR /usr/src/app

COPY . .

RUN apt-get update
RUN apt-get install nodejs -y
RUN MAKE="make --jobs $(nproc)" bundle install --jobs $(nproc)


EXPOSE 3000

CMD ["bundle", "exec", "rails", "s", "-b", "0.0.0.0"]
